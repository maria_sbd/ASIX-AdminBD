= Activitat: CMS dinàmics
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:
:icons: font

== Introducció

Els CMS dinàmics són aquells que utilitzen webs generades dinàmicament
a través de l'execució de scripts en el servidor i l'enviament dels
resultats d'aquesta execució als clients, i que utilitzen una base de dades
per guardar la informació que s'hi mostrarà.

En aquesta pràctica veurem l'ús bàsic d'un dels CMS dinàmics més populars, el
link:https://wordpress.org/[wordpress].

1. A part de `wordpress`, cerca almenys tres CMS dinàmics populars i inclou
l'adreça de la seva pàgina web. Quin llenguatge de programació utilitzen?

2. Quina és la diferència entre un _framework_ web i un CMS dinàmic?

== Instal·lació

En aquest apartat crearem un script que realitzi una instal·lació desatesa de
Wordpress a una màquina virtual vagrant.

L'objectiu és que a partir d'una instal·lació de Debian bàsica es realitzin de
forma automàtica tots els passos necessaris per obtenir un Wordpress funcional
sense que l'usuari hagi d'interactuar en cap moment amb el sistema.

Per aquest apartat pots consultar la
link:https://wordpress.org/support/article/how-to-install-wordpress/[documentació d'instal·lació oficial]
i la guia
link:https://peteris.rocks/blog/unattended-installation-of-wordpress-on-ubuntu-server/[Unattended installation of WordPress on Ubuntu Server].

[start=3]
3. Quins són els prerequisits per instal·lar l'última versió estable de
Wordpress? Anota el programari i les versions necessàries.

4. Tenint en compte els requeriments anteriors, crea el fitxer de configuració
de Vagrant per crear una màquina amb la versió necessària de Debian.

En els següents apartats poblarem el fitxer `bootstrap.sh` amb les instruccions
necessàries per a completar la instal·lació de Wordpress.

Servirem el Wordpress des del directori `/var/www/virtual/wordpress`.

[start=5]
5. Afegeix les instruccions necessàries per a instal·lar els prerequisits de
Wordpress. Comprova que l'script funciona.

6. Afegeix les instruccions necessàries per a configurar l'Apache per tal que
l'arrel de la web sigui el directori `/var/www/virtual`.

7. Afegeix les instruccions necessàries per a crear la base de dades de
Wordpress i l'usuari que hi accedirà.

8. Afegeix les instruccions necessàries per baixar l'última versió de Wordpress
i descomprimir el fitxer.

9. Afegeix les instruccions necessàries per generar el fitxer `wp-config.php`
amb la configuració adequada. Utilitza l'adreça
https://api.wordpress.org/secret-key/1.1/salt/ per obtenir unes cadenes de
_sal_ aleatòries.

10. Afegeix les instruccions necessàries per completar l'última part de la
instal·lació, que habitualment es fa via web, accedint a l'adreça web
`/wp-admin/install.php?step=2` de la teva instal·lació i passant-li les dades
que requereix.

11. Comprova que tot funciona i que quan ha arrencat la màquina tenim una
instal·lació de Wordpress funcional.

== Ús

En aquest apartat utilitzarem l'entorn web del Wordpress per crear un esquelet
de web funcional sobre el projecte que realitzareu a final de curs. Podeu
treballar individualment o en els grups de projecte.

=== Usuaris i rols

En cada projecte els usuaris que hi haurà seran diferents, però sempre
serà important tenir almenys un usuari que no tingui permisos d'administració
per tal de poder editar articles sense risc de desconfigurar res.

[start=12]
12. Indica quins són els rols que hi ha a Wordpress i quina utilitat té cada un.
Observa la diferència entre un editor i un autor. De cara a la documentació del
teu projecte, fes un esquema dels usuaris que podran accedir a la teva web, el
rol que tindrà cada un i els permisos que els hi donaràs. Crea aquests
usuaris/rols.
+
Webs de referència: link:https://wordpress.org/support/article/administration-screens/#users-your-blogging-family[Usuaris].

=== Posts i Pages

[start=13]
13. La teva web ha de contenir més d'una pàgina (_page_) i almenys una d'elles
ha de ser tipus _ABOUT_. Crea un menú que inclogui les pàgines d'accés. Els
posts han d'estar classificats per categories. Fes un esquema de les categories
que inclouràs.
+
Investiga què és un _permalink_ i configura l'Apache per tal que faci servir
`pretty permalinks`. Les teves pàgines han de ser permalink.
+
Inclou una captura de les diferents pàgines que heu creat, en que es vegi el
_pretty permalink_.
+
Webs de referència:
+
--
link:https://wordpress.org/support/article/wordpress-lessons/[WordPress Lessons]
link:https://wordpress.org/support/article/wordpress-editor/#blocks[Blocks]
link:https://wordpress.org/support/article/using-permalinks/[Using Permalinks]
link:https://wordpress.org/support/article/pages/[Pages]
link:https://codex.wordpress.org/Creating_Horizontal_Menus[Creating Horizontal Menus]
--

=== Extensions

Wordpress és principalment un gestor de blogs o revistes. El que ha fet que es
convertís en un CMS complet és la capacitat de poder-lo ampliar i modificar
fàcilment, cosa que li confereix molta flexibilitat.

Utilitzarem tres tipus d'extensions:

- Idiomes
- Temes
- Plugins

==== Idiomes

[start=14]
14. Afegeix un altre idioma a la teva instal·lació de Wordpress. Comprova quin/s
idiomes tens instal·lats per defecte i consulta la documentació per veure com
s'ha de fer per instal·lar un nou idioma. Documenta el procés seguit.

15. Imaginem que volem crear un blog bilingüe: tots els articles estaran escrits
en dos idiomes. Quan entrin els usuaris volem que cadascun d'ells puguin triar
en quin idioma el veuen. És fàcil d'aconseguir? Documenta el procés. *(OPCIONAL)*

Per modificar l'idioma per defecte de Wordpress cal consultar la documentació a
link:https://wordpress.com/support/language-settings/[Configuració de l'idioma].
Cal tenir en compte que encara que el Wordpress estigui traduït a un idioma, no
significa que tots els plugins que instal·lem estiguin necessàriament traduïts.

També podem tenir un lloc amb Wordpress amb diversos idiomes, simultàniament:
link:https://wordpress.com/support/set-up-a-multilingual-blog/[Configurant un lloc multiidioma].

==== Temes

Un tema és bàsicament el disseny del teu lloc web. És la part de Wordpress que
s'encarrega de posicionar els nostres continguts a la pantalla, de forma
independent del contingut.

Amb un tema es pot personalitzar el lloc web. Els temes són relativament fàcils
de construir o adaptar i proporcionen la màxima flexibilitat per donar estil al
lloc.

[start=16]
16. Modifica algunes característiques dels temes que tenim instal·lats. Vés a
l'apartat de temes i comprova com es poden modificar els colors i els elements
(_widgets_) que apareixen a cadascuna de les pàgines.
+
Inclou captures indicant el tema i quins canvis s'han fet.

L'entorn del Wordpress només ens deixa fer alguns canvis als temes. Sovint ens
trobarem que necessitem fer més canvis que els que aquest entorn ens permet.
Aleshores caldrà que modifiquem els fitxers d'estil del tema en sí,
principalment el fitxer anomenat `style.css`.

La modificació d'aquest fitxer directament té alguns problemes: quan aparegui
una actualització del tema, si l'instal·lem, els nostres canvis es perdran. Per
això s'acostuma a treballar amb temes derivats (_child themes_).

[start=17]
17. Tria un dels temes bàsics que t'agradi i crea un tema derivat. Consulta
link:https://developer.wordpress.org/themes/advanced-topics/child-themes/[Child Themes]
per veure com es fa.
+
Un cop fet el tema derivat, comprova que la teva web es veu exactament igual amb
el tema original que amb el tema derivat.
+
A continuació, modifica algunes característiques d'estil del tema que acabes de
crear. Documenta els canvis que fas i inclou captures on s'apreciïn.
+
Aquestes webs (fetes amb Wordpress) contenen també bons tutorials:
--
- link:https://thethemefoundry.com/blog/how-to-customize-a-wordpress-theme/[How to Customize a WordPress Theme]
- link:http://themeshaper.com/modify-wordpress-themes/[Modify Wordpress Themes]
--

==== Plugins

Els plugins afegeixen funcionalitats noves al Wordpress. Hi ha plugins per a
gairebé qualsevol cosa que un es pugui imaginar. Amb l'ajuda de plugins podem
convertir fàcilment la nostra pàgina en un fòrum, una tenda virtual, o una wiki.

La potència dels CMS com Wordpress o Joomla! es deu bàsicament a la quantitat de
plugins disponibles que tenim.

A continuació trobes cinc exercicis dels que n'hauràs d'implementar almenys
*TRES* a la teva web.

[start=18]
18. Un dels plugins més recomanats és el
link:https://wordpress.org/plugins/wordpress-seo/[WordPress SEO].
+
Instal·la'l i llista cinc característiques que creguis que podrien millorar la
teva web.

19. Una utilitat habitual del WordPress és utilitzar-lo com una botiga virtual.
Hi ha molts plugins que permeten fer això. Si la teva pàgina és de serveis, pots
fer servir alguns d'aquests plugins per tal de fer els cobraments.
+
El plugin
link:https://wordpress.org/plugins/quick-paypal-payments/[Quick Paypal Payments]
permet rebre donacions a través del Paypal. Prova'l.

20. El plugin
link:https://wordpress.org/plugins/wordpress-simple-paypal-shopping-cart/[WordPress Simple Paypal Shopping Card]
converteix el nostre WordPress en una tenda.
+
També hi ha el plugin
link:https://wordpress.org/plugins/woocommerce/[WooComerce]
amb funcionalitat similar i que ha estat un plugin molt utilitzats per comerç
electrònic.
+
Prova un d'aquests dos últims plugins, o algun altre amb funcionalitat similar,
i escriu una valoració que comenti la seva funcionalitat. Pots trobar altres
plugins similars a
link:http://www.wpbeginner.com/plugins/10-wordpress-paypal-plugins-for-easily-accepting-payments/[10 WordPress Paypal plugins for easily accepting payments].

21. Una altra característica habitual és permetre reservar cites directament des
de la nostra web. Hi ha diversos plugins que permeten fer això:
+
link:http://www.wpbeginner.com/plugins/5-best-wordpress-appointment-and-booking-plugins/[5 Best Wordpress Appointment and Booking Plugins].
+
Pots provar, per exemple, el
link:https://wordpress.org/plugins/booking/[Booking Calendar].

22. Si permetem que els nostres usuaris puguin posar comentaris als nostres
posts, aviat ens trobarem amb el problema de l'spam. Hi ha robots que es
dediquen a cercar pàgines desprotegides i omplir-les d'spam automàticament. Una
de les alternatives per combatre això és amb un filtre anti-spam. Prova el
plugin link:https://wordpress.org/plugins/akismet/[Akismet] i intenta
comprovar-ne la seva funcionalitat (necessitaràs una clau per les API d'Akismet,
gratuïta).

[NOTE]
====
Opcionalment, podeu canviar algun d'aquesta plugins per algun altre que penseu
que pot ser interessant per la vostra web.
====

== Desplegament

L'objectiu d'aquest apartat és traspassar el vostre Wordpress al servidor en
producció.

Hi ha diverses formes de fer això, però com que ja sou gairebé administradors
podeu fer-ho de la forma que creieu més convenient.

[start=23]
23. Documenta els passos que has seguit per fer el desplegament. Inclou
referències a qualsevol documentació o tutorial que hagis utilitzat.

24. Inclou una captura de pantalla amb el Wordpress funcionant al servidor en
producció.
