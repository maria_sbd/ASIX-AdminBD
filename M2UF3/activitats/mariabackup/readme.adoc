= Activitat: còpies de seguretat i restauració amb Mariabackup
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:
:icons: font

== Ús de `Mariabackup`

El `Mariabackup` és una eina lliure proporcionada conjuntament amb el MariaDB
per realitzar còpies de seguretat físiques de taules InnoDB, Aria i MyISAM. Pel
cas d'InnoDB és possible realitzar còpies en calent.

1. El Mariabackup es proporciona en el paquet `mariadb-backup`, inclòs als
repositoris oficials. Instal·la la versió corresponent a la versió de MariaDB.

2. Crea un usuari específic per ser utilitzat pel Mariabackup. Dona-li els
privilegis `PROCESS`, `RELOAD`, `LOCK TABLES` i `REPLICATION CLIENT`.

3. Inclou la secció `[xtrabackup]` al fitxer de configuració. Afegeix opcions
per indicar el nom d'usuari, la contrasenya, i el directori on es guarden les
dades.
+
[TIP]
====
El Mariabackup és una eina derivada de Percona XtraBackup, així que per a la
seva configuració utilitzarem la secció `[xtrabackup]`.
====

Quan utilitzem el Mariabackup, tenim l'opció de realitzar còpies de seguretat
completes o incrementals. Les còpies completes creen una còpia sencera a un
directori buit, mentre que les còpies incrementals actualitzen una còpia prèvia
amb les dades noves.

Començarem amb les còpies completes.

[start=4]
4. Crea una còpia de seguretat completa de la base de dades `sakila` al
directori `/var/mariabackup/full`. Per fer-ho, segueix les instruccions de
link:https://mariadb.com/kb/en/library/full-backup-and-restore-with-mariabackup/[Full backup and restore with Mariabackup]
i comprova les opcions disponibles a
link:https://mariadb.com/kb/en/library/mariabackup-options/[Mariabackup opcions].

5. Elimina la base de dades `sakila`.

6. Abans de restaurar la base de dades cal preparar els fitxers de còpia, per
evitar inconsistències degut als canvis que s'han pogut fer a la base de dades
mentre s'estava fent la còpia de seguretat. Utilitza l'opció `--prepare` per
fer-ho.

7. Para el servidor MariaDB i restaura la base de dades `sakila` utilitzant
l'opció `--copy-back` de Mariabackup. Com que només estem restaurant una base
de dades i Mariabackup demana fer la restauració en un directori buit, haurem
de restaurar a un directori temporal i copiar els fitxers al directori de dades
després.

8. Assegura't que els fitxers que hem restaurat pertanyen a l'usuari i grup
del MariaDB.

9. Arrenca el servidor MariaDB i comprova que la restauració s'ha fet
correctament.

Per fer còpies incrementals, el Mariabackup aprofita que el motor InnoDB guarda
números de seqüència (LSN) per a totes les operacions que modifiquen alguna
taula. Quan es fa una còpia incremental, el Mariabackup comprova els LSN més
recents de les còpies de seguretat amb els que hi ha a la base de dades.
Aleshores, actualitza els fitxers de còpia que han quedat obsolets.

[start=10]
10. Modifica el títol de la pel·lícula amb `film_id` 1.

11. Partirem de la còpia de seguretat completa que hem creat anteriorment.
Comprova el contingut del fitxer `xtrabackup_checkpoints` de la còpia de
seguretat. Quin és l'últim LSN registrat?

12. Executa el Mariabackup amb les opcions `--backup` i `--incremental-basedir`
per actualitzar la nostra còpia de seguretat. Indica com a directori objectiu
`/var/mariabackup/inc1` Pots seguir l'exemple de
link:https://mariadb.com/kb/en/library/incremental-backup-and-restore-with-mariabackup/[Incremental backup and restore with Mariabackup].

13. Comprova el directori `inc1` que s'ha creat durant l'actualització de la
còpia de seguretat. Examina el contingut del fitxer `xtrabackup_checkpoints`.
Quin és el rang de LSN registrats en aquest directori?

14. Repeteix el procés dels últims apartats: modifica alguna dada de la base
de dades, i crea una còpia incremental a partir de la còpia de l'apartat
anterior dins del directori `/var/mariabackup/inc2`.

15. Elimina la base de dades `sakila`.

16. Prepara els fitxers de còpia de seguretat per a ser restaurats. Necessitem
aplicar les diferències incrementals a la còpia completa. Utilitza
`--prepare` a la còpia completa i a cadascuna de les incrementals.

17. Utilitza `--copy-back` per restaurar la còpia de seguretat, com hem fet
abans amb la còpia completa.

18. Comprova que `sakila` s'ha restaurat i que hi ha els últims canvis que hi
havies fet.
