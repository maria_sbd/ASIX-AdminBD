= Activitat: còpies de seguretat i restauració amb `mysqldump`
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:
:icons: font

== Ús de `mysqldump`

Utilitzarem la utilitat `mysqldump` per crear una còpia de seguretat de la base
de dades `sakila`, i després restaurar-la en una altra base de dades.

1. Crea el directori `/var/dbbackup` i assegura't que el servidor MariaDB té
permisos per escriure-hi fitxers.

2. Utilitza `mysqldump` per crear una còpia de seguretat completa. Assegura't
que l'script generat per `mysqldump` inclou les sentències necessàries per crear
la base de dades.

3. Esborra completament la BD `sakila` i recrea-la a partir de la còpia de
seguretat.

4. Utilitza `mysqldump` per crear una còpia de seguretat, separada per
tabuladors, de la taula `film_actor`.

5. Quins fitxers ha generat l'ordre anterior? Què conté cadascun?

6. Esborra completament la taula `film_actor`.

7. Utilitza el client `mysql` per restaurar l'estructura de la taula
`film_actor` a partir de la còpia de seguretat parcial que hem fet.

8. Utilitza el client `mysqlimport` per carregar les dades de la taula.

9. Utilitza el client `mysql` per comprovar que la restauració s'ha fet
correctament.
