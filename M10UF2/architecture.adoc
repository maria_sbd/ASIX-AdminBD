= Arquitectura de MariaDB
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:

== Què és MariaDB

link:https://mariadb.com/kb/en/library/about-mariadb-software/[Sobre MariaDB]

== Executables instal·lats

Després de la instal·lació del MariaDB podem trobar:

- El servidor de MariaDB.
- Programes clients.
- Altres programes (no clients).

MariaDB utilitza una arquitectura client-servidor. El programa client pot
estar a la mateixa màquina que el servidor o en una màquina diferent. En
aquest darrer cas, ambdues màquines poden executar sistemes operatius
diferents.

Els programes clients es connecten a un servidor MariaDB i permeten realitzar
operacions de consulta i modificació de dades, o manteniment del servidor.

Alguns dels programes client que podem trobar després de la instal·lació són:

- `mysql` (`/usr/bin/mysql`): permet executar sentències SQL contra el servidor,
de forma interactiva, en línia d'ordres.
- `mysqladmin` (`/usr/bin/mysqladmin`): permet realitzar operacions d'administració,
com comprovar la configuració del servidor, o crear i eliminar bases de dades.
- `mysqlcheck` (`/usr/bin/mysqlcheck`): s'utilitza per comprovar i reparar taules.
- `mysqldump` (`/usr/bin/mysqldump`): s'utilitza per realitzar còpies de seguretat
de bases de dades. Típicament, les còpies es guarden en forma de sentències SQL
capaces de recrear l'estructura de la base de dades i poblar-la amb les dades
originals.
- `mysqlimport` (`/usr/bin/mysqldump`): aquest programa permet importar dades des de
fitxer de text pla.
- `mysqlshow` (`/usr/bin/mysqlshow`): mostra informació sobre quines bases de dades,
taules, i columnes existeixen al servidor.
- `mysqlslap` (`/usr/bin/mysqlslap`): és un programa de diagnòstic que permet
simular diversos nivells de càrrega contra el servidor per veure com respon.

A més, podem instal·lar programes clients addicionals, per exemple:

- `OmniDB` o `DBeaver`: són programes clients multiplataforma i multi-SGBD que
permeten executar sentències SQL i estudiar el disseny de les bases de dades des
d'un entorn gràfic.
- `mycli`: és similar al client per defecte `mysql`, però incorpora autocompleció
i ressaltat de sintaxi.

A link:https://mariadb.com/kb/en/library/clients-utilities/[Clients & Utilities]
podem trobar una llista més completa dels programes clients per MariaDB.

A més dels programes clients, tenim una colla de programes d'utilitat que
actuen directament contra els fitxers on es guarden les bases de dades.

[TIP]
====
Els programes clients s'han de connectar a un servidor MariaDB per poder
funcionar. En canvi, les utilitats treballen directament contra els fitxers on
es guarden les bases de dades.
====

Típicament, els programes d'utilitat hauran de funcionar amb el servidor parat
(per evitar que els fitxers siguin modificats mentre hi estan treballant).

Alguns programes d'utilitat són:

- `myisamchk`: comprova i repara taules en format MyISAM.
- `myisampack`: crea una versió comprimida de només lectura de taules MyISAM.
- `innochecksum`: comprova la integritat de les taules InnoDB.
- `mysqlaccess`: comprova permisos d'accés.
- `mysqldumpslow`: mostra un resum dels registres de consultes lentes.

[TIP]
====
Alguns d'aquests programes poden arribar a fer malbé els fitxers de les bases
de dades. És convenient llegir el manual i fer una còpia de seguretat dels
fitxers quan així es recomani.
====

== El servidor `mysqld`

L'executable del servidor del MariaDB es diu `mysqld` i el podem trobar a
`/usr/sbin/mysqld`.

Després de la instal·lació, el servidor està configurat per arrencar automàticament.

En un mateix servidor (_host_) es poden executar diverses instàncies del
servidor `mysqld`.

El servidor gestiona l'accés a les bases de dades, a disc o a la memòria,
i accepta múltiples clients simultanis.

El MariaDB suporta diversos motors per a les taules d'una base de dades. Cada
motor té diverses característiques: alguns són transaccionals i alguns no.
Taules diferents poden utilitzar motors diferents, encara que estiguin a la
mateixa base de dades. Per defecte, el motor que s'utilitza és l'InnoDB.

El servidor `mysqld` és un únic procés al sistema operatiu, i utilitza
diversos fils d'execució (_threads_):

[source,bash]
----
$ ps -C mysqld
  PID TTY          TIME CMD
  911 ?        00:00:06 mysqld
$ ps -T -p 911
  PID  SPID TTY          TIME CMD
  911   911 ?        00:00:00 mysqld
  911   947 ?        00:00:00 mysqld
  911   973 ?        00:00:00 mysqld
...
  911  1061 ?        00:00:00 mysqld
  911  1064 ?        00:00:00 mysqld
  911  1069 ?        00:00:00 mysqld
----

Els servidor `mysqld` s'organitza en les següents capes:

image:images/basic_architecture.png[Arquitectura bàsica]

- La _capa de connexió_ valida els usuaris. Es poden utilitzar diversos mecanismes
i plugins per fer la validació. Per exemple, es pot fer que els usuaris i
contrasenyes vàlids estiguin guardats en una base de dades, o que s'utilitzi
LDAP per a la validació.

- La _capa SQL_ processa les sentències SQL. Això implica tasques com la
comprovació dels permisos de l'usuari, l'optimització de les consultes, la
consulta a caus (_caches_), i l'execució de la sentència.

- La _capa d'emmagatzematge_ és l'encarregada d'accedir físicament a les dades
sol·licitades, o de fer-ne les modificacions oportunes. Depenent de la
configuració i del motor utilitzat, accedirà a un o diversos fitxers, a la
memòria, o a dispositius d'emmagatzematge en xarxa.

== Tipus de connexió i autenticació

link:https://mariadb.com/kb/en/library/pluggable-authentication-overview/[Plugins d'autenticació]
