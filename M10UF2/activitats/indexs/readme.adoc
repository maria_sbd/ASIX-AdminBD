= Activitat: índexs
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:

Utilitzarem la màquina Vagrant de la pràctica d'optimització de consultes.

== Índexs

1. Anotar la mida dels fitxers on es guarda la base de dades `hotel`.

2. Fer aquestes consultes i anotar el temps que tarden.
+
- Hostes el nom dels quals comença per J, ordenats per nom.
- Hostes el número de document dels quals comença per 4, ordenats pel número
de document.
- Hostes el número de document dels quals acaba per 4, ordenats pel número
de document.

3. Utilitza link:https://mariadb.com/kb/en/library/explain/[EXPLAIN] i
link:https://mariadb.com/kb/en/library/analyze-statement/[ANALYZE] sobre les
consultes anteriors.

4. Explica el significat de cadascuna de les columnes que apareixen. Indica
quines de les consultes utilitzaven índexs i quines no.

5. Cerca quants hostes hi ha de cada nacionalitat. Utilitza ANALYZE i indica
el significat del contingut de la columna `Extra`. Utilitza
ANALYZE FORMAT=JSON per obtenir més informació. Per què creus que surt el que
surt a `Extra`?

6. Volem anar de cap de setmana. Cerca per quins tipus d'habitacions queden
habitacions lliures pels dies de l'1 al 4 de novembre.
+
[TIP]
====
Mira quines habitacions hi ha reservades aquests dies, i resta-les del conjunt
de totes les habitacions que hi ha a l'hotel. Després, agrupa per tipus
d'habitació.
====

7. Utilitza ANALYZE sobre la consulta anterior. Quins índexs s'estaven utilitzant
per accelerar la consulta? Anota el temps que ha tardat a realitzar-se.

8. Elimina els índexs de la taula `BookingCalendar`. Per fer-ho, caldrà
eliminar-ne la clau primària i les claus foranes. Repeteix la consulta anterior,
i compara el temps que tarda ara i els plans d'execució.

9. Consulta la documentació sobre
link:https://mariadb.com/kb/en/library/getting-started-with-indexes/[índexs].
+
- Per què és important que la clau primària ocupi tants pocs bits com sigui
possible?
- Quins tipus d'índexs hi ha? Indica la diferència principal entre cadascun
d'ells.

10. Torna a comprovar la mida dels fitxers de la base de dades. Quins són
diferents?

11. Mostra el nom i cognoms de tots els hostes que estaven allotjats a l'hostal
la nit de l'1 al 2 d'agost de 2018.

12. Utilitza ANALYZE sobre la consulta anterior. En quins passos s'utilitzen
índexs?

13. Per a cadascuna de les següents consultes, cerca quin o quins índexs són els
millors per accelerar-les:
+
[TIP]
====
Per a aquest exercici és recomanable recrear la màquina virtual i assignar-li
dues GB de memòria. També és convenient eliminar els índexs creats a cada
apartat per no interferir als apartats següents. Recorda també de desactivar
la memòria cau.
====
+
--
a.  Per cada nacionalitat, cerca la mitjana de dies de durada de les seves reserves.
+
[source,sql]
----
SELECT c.Nationality, AVG(DATEDIFF(CheckOut, CheckIn))
FROM Bookings b
JOIN Customers c ON c.Id = b.CustomerId
GROUP BY c.Nationality
----

b. Quins clients han fet més d'una reserva a l'hotel, de les quals almenys una
hagi estat cancel·lada? Mostra'n el nom complet i quantes reserves han fet.
+
[source,sql]
----
SELECT FirstName, LastName, COUNT(*) AS NumReserves
FROM Customers c
JOIN Bookings b ON b.CustomerId = c.Id
GROUP BY c.Id, c.FirstName, c.LastName
HAVING GROUP_CONCAT(b.State) LIKE '%Cancelled%' AND NumReserves>1
----

c. Per cada nacionalitat, mostra el nom de pila més comú a la taula d'hostes.
+
[source,sql]
----
SELECT Nationality, COUNT(*) AS recompte, FirstName
FROM Hosts
GROUP BY Nationality, FirstName
HAVING (Nationality, recompte) IN
(
	SELECT Nationality, MAX(num) FROM (
		SELECT COUNT(*) AS num, FirstName fn, Nationality nat
		FROM Hosts
		GROUP BY FirstName, Nationality
	) AS t
	JOIN Hosts h ON h.Nationality = nat
	GROUP BY Nationality
)
----
--
