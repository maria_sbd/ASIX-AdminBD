= Instal·lació i actualització de MariaDB
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

== Instal·lació

Hi ha dues opcions per instal·lar el MariaDB en Debian:

1. Instal·lar el paquet inclòs a la distribució.
2. Instal·lar el paquet preparat pels desenvolupadors de MariaDB per a Debian.

Per exemplificar els problemes habituals que comporta la instal·lació i
actualització de serveis, primer instal·larem la versió de la distribució i
després actualitzarem a l'última versió estable disponible a la web de MariaDB.

Debian té un cicle de desenvolupament relativament lent (uns dos anys per a
cada distribució estable). Això és ideal per un servidor perquè implica que el
sistema serà molt estable durant aquest període, sense renunciar a aplicar les
actualitzacions de seguretat que van apareixent.

Ens podem trobar però que hi hagi alguna característica nova que ens resulti
interessant a les versions més recents, no incloses a Debian.

[NOTE]
====
El primer que farem en aquests casos és comprovar si existeix una versió més
recent al repositori de _backports_ de Debian.
====

El MySQL i el MariaDB són dos SGBD molt similars, fins al punt que comparteixen
el nom dels fitxers que instal·len. Per això, tot i que podem optar per
instal·lar un o l'altre, no podrem instal·lar els dos paquets simultàniament.

*Qüestió 1*: Quina és la versió de MySQL disponible a la distribució estable de
Debian? Quina és la versió de MariaDB?

Per facilitar la instal·lació, a Debian han creat el paquet `default-mysql-server`
que farà que s'instal·li en cada moment la versió proposada de MySQL/MariaDB.

*Qüestió 2*: Sense fer encara la instal·lació, quin sistema i versió
s'instal·larà si instal·lem el paquet `default-mysql-server`?

Com que farem diverses proves i configuracions diferents en aquestes pràctiques,
utilitzarem màquines virtuals netes cada cop. Per estalviar temps, ho farem
amb el `vagrant`, un programa capaç de recrear màquines virtuals
automàticament a partir d'un fitxer de configuració. Cada pràctica incorporarà
el fitxer `Vagrantfile` necessari per configurar el servidor a l'estat inicial
de la pràctica.

*Qüestió 3*: Reinicia l'ordinador, vés a la BIOS i comprova si la tecnologia
d'acceleració de màquines virtuals està activada. Si no ho està, activa-la.

*Qüestió 4*: En línia d'ordres, situa't en aquest directori i executa
`vagrant up` per crear i arrencar la màquina virtual. Després, executa
`vagrant ssh` per establir una connexió SSH cap a la màquina virtual.

*Qüestió 5*: Instal·la la versió de MariaDB inclosa a Debian.

Un cop instal·lat el servidor podem comprovar que s'està executant amb:

[source,bash]
----
$ sudo systemctl status mysql
● mysql.service - LSB: Start and stop the mysql database server daemon
   Loaded: loaded (/etc/init.d/mysql)
   Active: active (running) since dt 2016-09-20 10:38:14 CEST; 8min ago
  Process: 711 ExecStart=/etc/init.d/mysql start (code=exited, status=0/SUCCESS)
   CGroup: /system.slice/mysql.service
           ├─820 /bin/bash /usr/bin/mysqld_safe
           ├─821 logger -p daemon.err -t /etc/init.d/mysql -i
           ├─974 /usr/sbin/mysqld --basedir=/usr --datadir=/var/lib/mysql --p...
           └─975 logger -t mysqld -p daemon.error
----

Tenim les següents operacions bàsiques:

- Parar el servidor: `sudo systemctl stop mysql`.
- Arrencar el servidor: `sudo systemctl start mysql`.
- Reiniciar el servidor: `sudo systemctl restart mysql`.
- Fer que el servidor no arrenqui per defecte quan engeguem l'ordinador:
`sudo systemctl disable mysql`.
- Fer que el servidor torni a arrencar per defecte: `sudo systemctl enable mysql`.

Un cop instal·lat el servidor, només l'usuari administrador del sistema
operatiu (_root_) podrà connectar-s'hi.

Per establir una connexió amb aquest usuari farem:

[source,bash]
----
$ sudo mysql
----

[NOTE]
====
Noteu que el programa `mysql` que estem invocant per iniciar una sessió és el
programa client que ens permet introduir ordres al servidor de MariaDB. El
servidor, que internament té de nom `mysqld`, s'arrenca automàticament quan
engeguem l'ordinador.
====

*Qüestió 6*: Estableix una connexió i apunta la versió que s'està executant
de MariaDB.

== Importació d'una base de dades

Ara importarem una base de dades al servidor. Baixa't la base de dades
link:http://downloads.mysql.com/docs/sakila-db.tar.gz[sakila] al mateix
directori on hi ha el Vagrantfile. Descomprimeix-lo. Executa `vagrant rsync`.

Connectem ara a la màquina virtual i anem al directori `/vagrant`, on trobarem
els fitxers que acabem de baixar.

Connectem a MariaDB i importem els fitxers de la base de dades, primer
`sakila-schema.sql` (l'estructura) i després `sakila-data.sql` (les dades).
Per importar un fitxer SQL utilitzem `source fixer.sql`.

*Qüestió 7*: Per comprovar que la base de dades s'ha importat bé, comprova
quantes pel·lícules (taula `film`) hi ha a la base de dades. Primer, executa
`use sakila` per passar a utilitzar aquesta base de dades.

== Actualització

Ara passarem a actualitzar MariaDB a l'última versió estable disponible.

*Qüestió 8*: Comprova si existeix una versió més recent de MariaDB a Debian
Backports. Si existeix, quina versió hi ha?

Instal·larem el paquet oficial de la web de MariaDB.

*Qüestió 9*: Configura el repositori oficial de MariaDB per a l'última versió
estable al teu sistema. Volem un repositori, no descarregar-nos directament
un paquet. Parteix de link:https://mariadb.org/[la pàgina oficial de MariaDB].

*Qüestió 10*: Cerca el registre de canvis de MariaDB a la web i indica dos
canvis que pensis que poden ser interessants entre la versió que hi ha a Debian
i l'última versió estable. *Cal entendre què són aquests canvis*.

[NOTE]
====
Noteu que l'usuari _root_ del sistema operatiu és l'únic que es pot connectar
al MariaDB, i ho fa com l'usuari _root_ de MariaDB, sense utilitzar contrasenya.

A més, aquest usuari només es pot connectar des del mateix _host_ on hi ha el
servidor.
====

Consulta ara la documentació sobre
link:https://mariadb.com/kb/en/library/upgrading/[actualització de MariaDB] i
ves a l'apartat corresponent a l'actualització que estem realitzant.

*Qüestió 11*: Llegeix el procediment recomanat per actualitzar el servidor de
MariaDB. Què no hem fet del tot bé?

*Qüestió 12*: Segons la documentació encara ens falta un pas per acabar
l'actualització. Realitza'l.

Passa ara a l'apartat de la documentació que parla del
`unix_socket_authentication_plugin`.

*Qüestió 13*: Ara mateix, tenim aquest plugin instal·lat?

*Qüestió 14*: Com es fa en MariaDB per instal·lar o desinstal·lar un plugin?

== Creació d'usuaris no administradors

Un cop hem creat una base de dades ja no voldrem treballar amb l'usuari
administrador (`root`).

El que farem serà crear un usuari que tingui tots els permisos sobre la base
de dades que hem creat, però que no pugui realitzar tasques administratives.

Això ens evita el risc de cometre errades i fer malbé el sistema.

*Qüestió 15*: Crea un usuari al sistema operatiu amb el teu nom. Fes que aquest
usuari, un cop validat en el sistema operatiu, pugui connectar al MariaDB i
tingui tots els privilegis sobre la base de dades `sakila` sense haver
d'introduir una altra contrasenya.

*Qüestió 16*: Crea ara un usuari amb el teu cognom, però que no sigui del
sistema operatiu, sinó un usuari només de MariaDB. Fes que necessiti una
contrasenya per autentificar-se al MariaDB i que tingui tots els privilegis
sobre la base de dades `sakila`.

Finalment, ja podem tancar la sessió al `mysql`:

[source,sql]
----
> exit;
----

== Ordres habituals

Per connectar amb el servidor amb un usuari que necessita contrasenya
utilitzarem l'ordre següent:

[source,bash]
----
$ mysql -u joan -p
----

L'opció *-u* permet indicar amb quin usuari connectem, i l'opció *-p* indica
que utilitzarem una contrasenya per autenticar-nos en el servidor.

A continuació es presenten algunes ordres bàsiques per moure'ns en el sistema.

- `show databases;`: mostra una llista amb totes les bases de dades disponibles
al servidor. Com a mínim trobarem les bases de dades _mysql_ (on es guarda la
informació de tots els usuaris i permisos del servidor), _information_schema_
i _performance_schema_.

- `use <base de dades>;`: passem a utilitzar una de les bases de dades que hi
ha en el SGBD.

- `show tables;`: un cop estem utilitzant una base de dades concreta, amb
aquesta ordre podrem veure totes les taules que hi ha en aquesta BD.

- `desc <taula>;`: amb aquesta ordre podem veure l'estructura d'una de les
taules: quines columnes té i de quin tipus són, entre d'altres propietats.

- `select * from <taula>;`: aquesta és la primera sentència pròpiament SQL que
executem. Mostrarà el contingut complet de la taula que indiquem.

- `exit`: amb _exit_ o amb Ctrl+D tanquem la connexió al servidor.

Cal tenir en compte que la major part d'ordres han d'acabar en punt i coma. Si
no es posa, el sistema pensarà que encara no hem escrit l'ordre sencera, ja que
és habitual que les ordres SQL ocupin més d'una línia. Llavors, tenim prou amb
escriure el punt i coma que ens hem deixat a la línia següent per poder
continuar:

[source,sql]
----
MariaDB [mysql]> select * from user
    -> ;
----
