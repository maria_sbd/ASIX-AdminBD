<?php
require_once 'Connection.php';

session_start();

try {
  if (!isset($_GET['roomNumber'])) {
    throw new Exception("Falten paràmetres.");
  }
  $roomNumber = trim($_GET['roomNumber']);
  $conn = connect();
  $statement = $conn->prepare("DELETE FROM Rooms WHERE RoomNumber=:roomNumber");
  $statement->bindParam(':roomNumber', $roomNumber);
  $statement->execute();
  $_SESSION['success'] = 'Habitació eliminada correctament.';
  header('Location: index.php');
  exit();
} catch (Exception $e) {
  $_SESSION['error'] = $e->getMessage();
  header('Location: index.php');
  exit();
}

?>
